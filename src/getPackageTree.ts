import * as fs from 'fs';
import getPackageType from './getPackageType';

/** Iteratively builds the package tree */
export async function getPackageTree (
  pkgPath: string,
  readPackageMeta: (pkgPath: string) => any
) {

  const packageData = await readPackageMeta(pkgPath);
  const meta = {
    author: packageData.author,
    name: packageData.name,
    repo: packageData.repo,
  };

  const tree = {
    children: [],
    meta,
  };

  for await (const file of getPackageTree(path, isPackageJSON)) {
    tree[]
  }
};
