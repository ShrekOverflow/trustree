import { stat } from "fs";
import { join } from "path";
import { promisify } from "util";

async function checkNodePackage(packagePath: string) {
  const fullPath = join(packagePath, 'package.json');
  return (await statAsync(fullPath)).isFile();
}

const statAsync = promisify(stat);
const checks = new Map();
checks.set('node', checkNodePackage);

export default async function getPackageType(packagePath: string) {
  for(const [type, check] of checks) {
    if (await check(packagePath)) {
      return type;
    }
  }
}
