import * as fs from 'fs';
import * as pathUtils from 'path';
import * as util from 'util';

const readFileAsync = util.promisify(fs.readFile);

export default async function readPackageJSON(path: string) {
  const fullPath = pathUtils.join(path, 'package.json');
  const resolvedPath = require.resolve(path);
  const contents = await readFileAsync(resolvedPath, 'utf8');
  const packageParsed = JSON.parse(contents);
  return packageParsed;
}
