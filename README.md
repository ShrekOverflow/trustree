Simple package to check how many packages are signed over github for any given npm project. This was created out of curiosity as to how many people actually sign their packages. Please note that this isn't verifying the GPG Signature as there is no way to validate the repositories GPG Signature with the version publish on NPM this is just a tool I built to experiement. The metric for a package is measured by the package. I eventually want to convert this into a badge which gives a trustability metric for the repositories code base.

```
trust = ((signed(self)) ?+ ∑ (trust(dependencies))) / (num(dependencies) + 1);
```

For sake of completeness I am escalating the unsigned ness of a depth package.

```bash
# Check for current package 
$ trustree .
# Check for a given package
$ trustree npm

# Check for a specific version of a given package
$ trustree npm@0.0.1

# Check for dev dependencies aswell
$ trustree --dev [packageName]
```

Output using `npx` to execute the command without installing the package.

```bash
$ npx trustree kappa
Signed: 100%
kappa ✅
|- alpha ✅
|- beta ✅
   |- gamma ✅

$ npx trustree badkappa
Signed: 0%
kappa ❌
|- alpha ✅
|- beta ✅
   |- gamma ❌

$ npx trustree shadykappa
Signed: 83%
kappa ✅
|- alpha ✅
|- beta ✅
   |- gamma ❌
```
